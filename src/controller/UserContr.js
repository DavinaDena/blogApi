import { Router } from "express";
import { User, userSchema } from "../entity/User";
import { UserRepository } from "../repository/UserRepo";
import bcrypt from 'bcrypt';
import { generateToken, protect } from "../utils/token";
import passport from "passport";



export const userController = Router();


/**
 * route to add a new user (register)
 */
userController.post('/register', async (req, res)=>{
    try {
        //Using joi - we have stablish some requirements on the entity
        //for the registration of a new user
        //if the user doesnt meet the requirements then we dont even let
        //the info circulate since there's no need to send shitty information
        //to the back end
        //also we use req.body and not user because at this point we check
        //what the user wrote.. if valid then he can become a user and send it 
        //to our table
        const {error}= userSchema.validate(req.body, {abortEarly:false})
        if(error){
            res.status(400).json({error: error.details.map(item => item.message)})
            return
        }

        const newUser = new User();
        Object.assign(newUser, req.body)
        
        
        const exists = await UserRepository.findByEmail(newUser.email);
        if(exists){
            res.status(400).json({error: 'email already taken'})
            return;
        }

        newUser.role= 'user';
        newUser.password= await bcrypt.hash(newUser.password, 11)
        

        await UserRepository.addUser(newUser);

        //if the info arrives here it means we have a new user so now we are
        //giving him a token right away
        //the token is based on the user email, id and the role but
        //we can only base it on the id and username or just simply id
        //if we want to keep the info as safe as possible
        res.status(201).json({
            user: newUser,
            token: generateToken({
                email: newUser.email,
                id: newUser.id,
                role: newUser.role
            })
        });

    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})


/**
 * route for a login
 */
userController.post('/login', async (req, res)=>{
    try {
        
        const user = await UserRepository.findByEmail(req.body.email);
        if(user){
            const samePassword = await bcrypt.compare(req.body.password, user.password);

            if(samePassword){
                res.json({
                    user,
                    token: generateToken({
                        email: user.email,
                        id: user.id,
                        role: user.role
                    })
                });
                return;
            }
        }


        res.status(401).json({error: 'wrong email or password'})
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
        
    }
})

userController.get('/', protect(), async (req, res)=>{
    try {
        let user = await UserRepository.getAllUsers()
        res.json(user)
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})


/**
 * route to check if there is a token
 */
userController.get('/account', passport.authenticate('jwt', {session:false}), (req, res)=>{
    res.json(req.user);
})