import { Router } from "express";
import { PostRepository } from "../repository/PostRepo";
import passport from "passport";
import { Post } from "../entity/Post";
import { convertUTCDateToLocalDate } from "../utils/converteDate";


export const postController = Router();

/**
 * To add a post
 */
postController.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        //if we can we insert joi but right now its not so necessary on the post
        // const {error}= postSchema.validate(req.body, {abortEarly:false})
        // if(error){
        //     res.status(400).json({error: error.details.map(item => item.message)})
        //     return
        // }

        const newPost = new Post();

        Object.assign(newPost, req.body);

        let date = convertUTCDateToLocalDate(new Date());
        newPost.date = date.toISOString().slice(0, 19).replace('T', ' ');


        newPost.blogUser_id = req.user.id;
        await PostRepository.addPost(newPost);

        res.status(201).json(newPost);

    } catch (error) {
        console.log(error);
        res.status(400).end()
    }
})

/**
 * To delete a post
 */
postController.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        await PostRepository.deletePost(req.params.id)
        res.status(204).end()
    } catch (error) {
        res.status(500).json(error)
    }
})

/**
 * To edit a post
 */
postController.patch('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        let data = await PostRepository.findPostById(req.params.id)
        if (!data) {
            res.send(404).end();
            return
        }
        let update = { ...data, ...req.body };
        await PostRepository.updatePost(update, req.params.id);
        res.json(update)

    } catch (error) {
        console.log(error);
        res.status(400).end();

    }
})

//to get all posts
postController.get('/', async (req, res) => {
    try {
        let data;
        if (req.query.search) {
            data = await PostRepository.searchPost(req.query.search)
        } else {
            data = await PostRepository.getAllPost()
        }
        
        res.json(data)
    } catch (error) {
        res.status(500).json(error);
    }
})

postController.get('/byUser', async (req, res) => {
    try {
        let data = await PostRepository.postWithUser()
        res.json(data)
    } catch (error) {
        res.status(500).json(error);
    }
})

postController.get('/postByUser',passport.authenticate('jwt', {session:false}), async(req, res)=>{
    try {
        let data = await PostRepository.postsFromOneUser(req.user.id)
        res.json(data)
    } catch (error) {
        res.status(500).json(error)
    }
})
/**
 * On indique à passport que cette route est protégée par un JWT, si on tente d'y
 * accéder sans JWT dans le header ou avec un jwt invalide, l'accès sera refusé
 * et express n'exécutera pas le contenu de la route
 */
postController.get('/account', passport.authenticate('jwt', { session: false }), (req, res) => {
    //comme on est dans une route protégée, on peut accéder à l'instance de User correspondant
    //au token avec req.user
    res.json(req.user);
});
