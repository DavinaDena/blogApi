import express from 'express';
import passport from 'passport';
import cors from 'cors';
import { configurePassport } from './utils/token';
import { userController } from './controller/UserContr';
import { postController } from './controller/PostContr';

configurePassport();

export const server = express();


server.use(passport.initialize());

server.use(express.json());

server.use(cors());

server.use('/api/user', userController)
server.use('/api/post', postController)