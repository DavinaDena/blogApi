import { Post } from "../entity/Post";
import { User } from "../entity/User";
import { connection } from "./connection";




export class PostRepository{

    /**
     * 
     * @param {term} term 
     * @returns 
     */
    static async searchPost(term){
        const [rows] = await connection.execute('SELECT * FROM post INNER JOIN blogUser ON blogUser_id= userID WHERE CONCAT (title, text, date, postID, blogUser_id) LIKE ?', ['%'+term+'%'])
        let thePosts= [];
        for (let item of rows) {
            let post = new Post(item.title, item.text, item.date, item.postID, item.blogUser_id )
            post.blogUser= new User(item.username, item.email, item.password, item.role)
            thePosts.push(post)
            
        }
        return thePosts
    }

/**
 * Function to add a post
 * @param {post} post 
 * @returns the post
 */
    static async addPost (post){
        const [rows] = await connection.query('INSERT INTO post (title, text, date, postID, blogUser_id) VALUES (?,?,?,?,?)', [post.title, post.text, post.date, post.postID, post.blogUser_id])
        post.id= rows.insertId;
        return post;
    }

    /**
     * Function to find by id
     * @param {id} id 
     * @returns the post with the matching id
     */
    static async findPostById(id){
        const [rows] = await connection.execute('SELECT * FROM post WHERE postID=?', [id])
        return new Post(rows[0].title, rows[0].text, rows[0].date, rows[0].postID, row[0].blogUser_id)
    }

    /**
     * Function to update the post
     * @param {id} up 
     */
    static async updatePost(up, id){
        await connection.execute('UPDATE post SET title=?, text=?, date=? WHERE postID=?', [up.title, up.text, up.date, id])
    }

    /**
     * Function that deletes a post with matching id
     * @param {id} id 
     */
    static async deletePost(id){
        await connection.execute('DELETE FROM post WHERE postID=?', [id])
    }

    static async getAllPost(){
        const [rows]= await connection.execute('SELECT * FROM post');
        let allPosts= [];
        for (let item of rows) {
            let post = new Post(item['title'],item['text'], item['date'], item['postID'], item['blogUser_id'])
            allPosts.push(post)
        }
        return allPosts;
    }

    //recover posts with username
    static async postWithUser(){
        const [rows] = await connection.execute('SELECT * FROM post LEFT JOIN blogUser ON blogUser_id= userID;') 
        let thePosts= [];
        for (let item of rows) {
            let post = new Post(item.title, item.text, item.date, item.postID, item.blogUser_id )
            
            post.blogUser= new User(item.username, item.email, item.password, item.role)
            thePosts.push(post)
            
        }
        return thePosts
    }

    static async postsFromOneUser(id){
        const [rows] = await connection.execute('SELECT * FROM post WHERE blogUser_id=?', [id]) 
        let thePosts= [];
        for (let item of rows) {
            let post = new Post(item.title, item.text, item.date, item.postID, item.blogUser_id )
            
            thePosts.push(post)
            
        }
        return thePosts
    }
}