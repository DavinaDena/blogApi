import { User } from "../entity/User";
import { connection } from "./connection";


export class UserRepository{

/**
 * to add a new user
 * @param {User} user 
 */
    static async addUser (user){
        const [rows]= await connection.query('INSERT INTO blogUser (username, email, password, role) VALUES (?,?,?,?)', [user.username, user.email, user.password, user.role])
        user.id = rows.insertId;
    }

    /**
     * Function to find by email
     * @param {email} email 
     * @returns the user matching the email
     */
    static async findByEmail(email){
        const [rows] = await connection.query('SELECT * FROM blogUser WHERE email=?', [email])
        if(rows.length===1){
            return new User(rows[0].username, rows[0].email, rows[0].password, rows[0].role, rows[0].userID)
        }
        return null;
    }

/**
 * Function to update user
 * @param {id} up 
 */
    static async updateUser(up){
        await connection.execute('UPDATE blogUser SET username=?, email=?, password=?, role=? WHERE userID=?', [up.username, up.email, up.password, up.role, up.id])
    }


    static async getAllUsers (){
        const [rows] = await connection.execute('SELECT * FROM blogUser') 
        let theUsers= [];
        for (let item of rows) {
            let user = new User(item.username, item.email, item.password, item.role, item.userID)
            theUsers.push(user)
        }
        return theUsers
    }
}

//SELECT * FROM blogUser INNER JOIN post ON userID= blogUser_id;