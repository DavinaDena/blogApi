
import fs from 'fs';
import jwt from 'jsonwebtoken';
import passport from 'passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserRepository } from '../repository/UserRepo';


const privateKey = fs.readFileSync('config/id_rsa');
const publicKey = fs.readFileSync('config/id_rsa.pub');

/**
 * Function that creates a token based on the private key
 * @param {object} payload body of the token
 * @returns token
 */
export function generateToken(payload) {
    const token = jwt.sign(payload, privateKey, { algorithm: 'RS256', expiresIn: 60 * 60 });
    return token;
}

/**
 * function that config the token while
 * getting it from the header{"authorization":"Bearer leToken"}
 */
export function configurePassport() {
    passport.use(new Strategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: publicKey,
        algorithms: ['RS256']
    }, async (payload, done) => {
        //here we use the token and its payload and we compare it
        //to the user by its email
        try {
            const user = await UserRepository.findByEmail(payload.email);

            if (user) {

                return done(null, user);
            }

            return done(null, false);
        } catch (error) {
            console.log(error);
            return done(error, false);
        }
    }))

}

export function protect(role = ['any']) {

    return [
        passport.authenticate('jwt', {session: false}),
        (req,res,next) => {
            if(role.includes('any') || role.includes(req.user.role)) {
                next()
            } else {
                res.status(403).json({error: 'Access denied'});
            }
        }
    ]
}