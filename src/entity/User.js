import Joi from "joi";


export class User{
    username;
    email;
    password;
    role;
    id;
    constructor(username, email, password, role='user', id=null){
        this.username= username;
        this.email= email;
        this.password= password;
        this.role= role;
        this.id= id;

    }
    toJSON(){
        return{
            username: this.username,
            id: this.id,
            email: this.email,
            role: this.role
        }
    }
}

export const userSchema= Joi.object({
    username: Joi.string().alphanum().required(),
    email: Joi.string().email().required(),
    password: Joi.string().min(4).required()
})