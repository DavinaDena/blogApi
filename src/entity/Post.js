import Joi from "joi";


export class Post{
    title;
    text;
    date;
    postID;
    blogUser_id;
    blogUser;
    constructor(title, text, date, postID, blogUser_id){
        this.title= title;
        this.text= text;
        this.date= date;
        this.postID= postID;
        this.blogUser_id= blogUser_id;
    }
}

// export const postSchema= Joi.object({
//     title: Joi.string().required(),
//     text: Joi.string().required()
// })