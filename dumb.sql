DROP TABLE IF EXISTS blogUser;
CREATE TABLE blogUser(  
    userID int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    username VARCHAR(300) NOT NULL,
    email VARCHAR (255) NOT NULL UNIQUE,
    password VARCHAR (255) NOT NULL,
    role VARCHAR (255) NOT NULL
) default charset utf8 comment '';


DROP TABLE IF EXISTS post;

CREATE TABLE post(
    postID int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    title VARCHAR (300) NOT NULL,
    text TEXT NOT NULL,
    date DATE NOT NULL,
    blogUser_id INTEGER,
    FOREIGN KEY (blogUser_id) REFERENCES blogUser (userID) ON DELETE SET NULL
);

INSERT INTO post (title, text, date) VALUES 
('New receipts', 'hey there! Im going to share with you new things!', '2021-01-03');